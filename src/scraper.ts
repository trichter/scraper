import facebook from './sources/facebook.js'
import ical from './sources/ical.js'
import rosalux from './sources/rosalux.js'
import endegelaende from './sources/endegelaende.js'
import aglink from './sources/aglink.js'
import adi from './sources/adi.js'
import boell from './sources/boell.js'
import frauenkultur from './sources/frauenkultur.js'
import lixer from './sources/lixer.js'
import bund from './sources/bund.js'
import hdg from './sources/hdg.js'
import ostpassagetheater from './sources/ostpassagetheater.js'
import prozesswerkstatt from './sources/prozesswerkstatt.js'
import wpEventList from './sources/wp-event-list.js'
import wpEventOrganiser from './sources/wp-event-organiser.js'

import TrichterDB, { Event, TrichterDBBranch, ImageFile } from '@trichter/db'
import * as path from 'path'
import moment from 'moment'
import sharp from 'sharp'
import Logger from '@trichter/logger'
import type { ScrapeFunction } from './sources/common.js'
const log = Logger.default('trichter-scraper')

const repoPath = process.argv[2]
if (!repoPath || !repoPath.trim()) {
  log.error('no repopath provided')
  process.exit(1)
}
const db = new TrichterDB.default(repoPath)

const sources: {[key: string]: ScrapeFunction} = {
  facebook,
  ical,
  adi,
  lixer,
  boell,
  hdg,
  frauenkultur,
  rosalux,
  endegelaende,
  ostpassagetheater,
  aglink,
  bund,
  prozesswerkstatt,
  'wp-event-list': wpEventList,
  'wp-event-organiser': wpEventOrganiser,
}

// the try mode disables any reset & commit git operations
const tryMode = process.env.TRICHTER_TRY_MODE && process.env.TRICHTER_TRY_MODE === '1';

(async function main () {
  try {
    const start = Date.now()
    let branch: TrichterDBBranch
    const master = await db.checkoutBranch('master')

    log.info('checkout or create branch `scraping`')
    if (await db.branchExists('scraping')) {
      branch = await db.checkoutBranch('scraping')
    } else {
      branch = await master.fork('scraping')
    }

    if (!tryMode) {
      log.info('set `scraping` to `master`')
      await branch.reset('master')
      await branch.clean()
    }

    log.info('load files')
    await branch.loadData()
    log.debug(`loaded ${branch.events.length} events in ${branch.groups.length} groups`)

    log.info('scrape and commit events!')
    await scrape(branch, process.argv[3])

    if (!tryMode) {
    //   log.info('update master again')
    //   await master.pull('origin', 'master')

      log.info('merge updates from `scraping` into `master`')
      await master.pull('.', 'scraping')
    }

    const end = Date.now()
    log.info(`done in ${Math.round((end - start) / 1000)} seconds`)
  } catch (err) {
    log.error(err)
  }
  // a bug in gaze keeps the event loop busy
  setTimeout(() => {
    process.exit(0)
  }, 2000)
}())

async function scrape (branch: TrichterDBBranch, groupFilter?: string): Promise<void> {
  const initialEventCount = branch.events.length
  let scrapedGroupCount = 0
  for (const group of branch.groups) {
    if (!group.scrape) continue

    if (groupFilter && !group.key.toLowerCase().includes(groupFilter.toLowerCase())) continue
    // if(group.scrape.source === 'facebook') continue
    log.info(`get events for group ${group.key.bold}`)

    if (!sources[group.scrape.source]) {
      log.error(`source ${group.scrape.source} does not exist`)
      continue
    }

    try {
      const events = await sources[group.scrape.source](group.scrape.options)
      const exclude = group.scrape.exclude && group.scrape.exclude.length ? group.scrape.exclude.map(id => id.toString()) : [] // we use strings for .indexOf()
      log.info(`found ${events.length} events`)
      scrapedGroupCount++
  
      for (const event of events) {
        log.debug(`got event ${new Date(event.start).toISOString()} "${event.title.bold}" `)
  
        // blacklisted event?
        if (exclude.indexOf(event.id.toString()) !== -1 || (event.parentId && exclude.indexOf(event.parentId.toString()) !== -1)) {
          log.debug('event is blacklisted in the group config')
          continue
        }
  
        // more than 6 months in the future?
        if (moment(event.start).isAfter(moment().add(3, 'months'))) {
          log.debug('event is too far in the future')
          continue
        }

        if (moment(event.start).isBefore(moment())) {
          log.debug('event is in the past')
          continue
        }
  
        // does ist cost money?
        if (
          event.description &&
          event.description.match(/[0-9](\s*)(€|Euro|Ois|EUR|,-)|Teilnahmegebühr|Abendkasse|verkauf|VVK|Eventim|eventbrite|TixForGigs|Hardtix|Tickets at the door/i) &&
          !event.description.match(/Spendenempfehlung|Spendenbasis|freiwillige Spende|kostenlos|kostenfrei/i) &&
          !event.description.match(/Anmerkung.*trichter/) // whitelisted, see sources/prozesswerkstatt
        ) {
          log.debug('seems like the event costs money')
          continue
        }
  
        // group filter
        if (group.scrape.filter) {
          let filter = false
          for (const key in group.scrape.filter) {
            const regex = new RegExp(group.scrape.filter[key], 'i')
            if (!regex.test(event[key])) {
              filter = true
              break
            }
          }
          if (filter) {
            log.debug(`filter because it doesn't fulfill following requirement: ${JSON.stringify(group.scrape.filter)} `)
            continue
          }
        }
  
        const existing = branch.events.find(e => e.id === event.id && e.group === group)
  
        const filename = path.join(group.key, `${moment(event.start).format('YYYY-MM-DD')} ${event.title.replace(/([/?<>\\:*|":]|[\x00-\x1f\x80-\x9f]|^\.+$)/g, '').slice(0, 40)}.md`)
        const e = existing || new Event(filename)
  
        if (!existing) {
          e.id = event.id
          e.parentId = event.parentId
          e.group = group
        }
        e.title = event.title
        e.start = new Date(event.start)
        e.end = event.end ? new Date(event.end) : null
        e.locationName = event.locationName
        e.address = event.address
        e.link = event.link
        e.teaser = event.teaser ? event.teaser.replace(/(\n|\r)/mg, '') : null
        e.description = event.description?.trim()
  
        e.isCrawled = true
  
        if (event.image && event.imageData) {
          e.image = event.image
          const imageFilename = path.join(group.directory, 'images', event.image)
          const existingImage = branch.getOne(f => f.filename === imageFilename)
          if (!existingImage) {
            const image = new ImageFile(imageFilename)
            if (event.imageData.length > 100 * 1024) { // >100KB
              // resize image
              try {
                const resized = sharp(Buffer.from(event.imageData, 'base64'))
                  .resize(650, 240, {
                    fit: sharp.fit.cover
                  })
                image.setImage(await resized.toBuffer())
              } catch (err) {
                log.error('error while resizing image for ' + e.link, err)
              }
            } else {
              image.setImage(Buffer.from(event.imageData, 'base64'))
            }
            await branch.save(image)
          }
        }
        await branch.save(e)
        if (!tryMode) {
          // add and commit
          await branch.add(group.directory)
          await branch.commit(`[${group.key}] ${event.title}`)
        }
      }
    } catch(err) {
      log.error(err)
    }

  }
  if (groupFilter && scrapedGroupCount === 0) {
    log.error(`No group with a name including '${groupFilter}' found`)
  } else {
    const newEventsCount = branch.events.length - initialEventCount
    log.info(`scraping done. Found ${newEventsCount} new ${newEventsCount === 1 ? 'event' : 'events'}`)
  }
  branch.stopWatcher()
}
