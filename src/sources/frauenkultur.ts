import moment from 'moment'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'
import { ScrapeFunction, ScrapedEvent, fetchImage, html2markdown } from './common.js'
import * as crypto from 'crypto'

const log = Logger.default('trichter-scraper/frauenkultur')

function getHash (str: string): string {
    return crypto.createHash('sha1').update(str, 'utf8').digest('base64').replace(/[^a-zA-Z0-9]/g, '')
}

const crawl: ScrapeFunction = async (options?: {}) => {
    const res = await fetch('https://www.frauenkultur-leipzig.de/programm/')
    const $ = cheerio.load(await res.text())
    const events: ScrapedEvent[] = []

    const cal = $('.row-inner > .programm > *').get()


    const now = new Date
    let year = null

    const dates: Array<{time: moment.Moment, title: string, link: string}> = []
    for(let el of cal) {
        try {
            if(el.tagName == 'h1') {
                year = $(el).text().split(' ')[1]
            } else {
                if(!year) return
                const dateStr = $('.event-title > span:first-child', el).text()
                const title = $('.event-title > span:last-child', el).text()
                const dayAndTime = $('.event-day-and-time', el).text()
                const times = dayAndTime
                    .split(' | ')[1]
                    .replace(/Uhr/g, '')
                    .trim()
                    .split(' bis ')
                const d = dateStr.split('.')
                const start = moment(`${year}-${d[1]}-${d[0]} ${times[0]}`)
                const end = moment(`${year}-${d[1]}-${d[0]} ${times[1]}`)
                // const imageUrl = $('.event-description img', el).attr('src')
                $('.event-description > i.image-ratio', el).remove()
                $('.event-description > .event-day-and-time', el).remove()
                const description = html2markdown(
                    $('.event-description', el).html()
                    .replace(/<span class="caps">(.*?)<\/span>/g, '\$1')
                )
    
                const event:  ScrapedEvent = {
                    id: getHash(start.toISOString() + dayAndTime).slice(0,8),
                    title: title,
                    start: start.toISOString(),
                    end: end.toISOString(),
                    locationName: "Soziokulturelles Zentrum Frauenkultur",
                    address: "Windscheidstrasse 51",
                    link: 'https://www.frauenkultur-leipzig.de/programm/',
                    description: description
                }
                events.push(event)
            }
        } catch(err) {
            log.error(err)
        }

    }
    return events
}
export default crawl
