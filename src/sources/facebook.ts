import fetch from '../fetch.js'
import moment from 'moment'
import { ScrapeFunction, ScrapedEvent } from './common.js'
import Logger from '@trichter/logger'
const log = Logger.default('trichter-scraper/facebook')

const FB_API_URL = process.env.TRICHTER_FB_API_URL || 'https://proxy.trichter.cc'

const crawl: ScrapeFunction = async (options: {page_id: string|number}) => {
  if (!options || !options.page_id) throw new Error('option \'page_id\' must be provided')
  try {
    return await getEvents(options.page_id.toString())
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl

async function getEvents (pageId: string): Promise<ScrapedEvent[]> {
  log.debug(`get events for ${pageId}`)
  const res = await fetch(`${FB_API_URL}/fbpage/${pageId}`, {
    // @ts-ignore is timeout really not available?
    timeout: 2 * 60 * 1000 // 2min
  })
  if (res.status !== 200) {
    log.error(`could not get events for page ${pageId} (${await res.text()})`)
    return []
  }
  const eventsJson = await res.json() as any[]
  const events = []
  for (const e of eventsJson) {
    // TODO: store image
    events.push({
      group: null,
      id: e.id,
      parentId: e.parentId,
      title: e.title,
      start: moment(e.start),
      end: e.end ? moment(e.end) : null,
      address: e.address,
      locationName: e.locationName,
      link: e.link,
      image: e.image,
      imageData: e.imageData,
      teaser: e.teaser,
      description: e.description
    })
  }
  return events
}
