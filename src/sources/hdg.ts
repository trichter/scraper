import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'
import { ScrapeFunction, ScrapedEvent, fetchImage, scrapeIcal } from './common.js'

const log = Logger.default('trichter-scraper/hdg')

const crawl: ScrapeFunction = async (options: { location: string }) => {
    const events: ScrapedEvent[] = []
    const res = await fetch(`https://www.hdg.de/${options.location}/veranstaltungen`)
    const $ = cheerio.load(await res.text())
    const panels = $('.panel')
    for(let panel of panels.toArray()) {
        
        if(!$(panel).text().includes('Eintritt frei')) continue
        const link = $('.calendar-link > a', panel).last().attr('href')
        const id = link.split('/')[3]
        log.debug(`scrape ${id}`)

        const ical = await scrapeIcal('https://www.hdg.de'+link.replace(/veranstaltungen\//, 'veranstaltungen/ical/')+'/termin-speichern')
        const event = ical[0]
        if(!event) {
            log.error(`ical of https://www.hdg.de${link} doesn't contain a event`)
            continue
        }
        event.id = id
        event.title = event.title.replace(/ - $/, '')
        event.link = 'https://www.hdg.de'+link
        const subtitle = $('.panel-heading h6', panel)
        $('span', subtitle).text(`(${$('span', subtitle).text().trim()})`)
        event.teaser = subtitle.text().trim() || null
        event.address = 'Grimmaische Straße 6'

        const imgSrc = $('.calendar-image > img', panel).attr('src')
        if(imgSrc) {
            const i = await fetchImage(imgSrc)
            event.image = i.image
            event.imageData = i.imageData
        }
        events.push(event)
    }
    return events
}
export default crawl
