import moment from 'moment'
import { ScrapeFunction, ScrapedEvent, html2markdown } from './common.js'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import { XMLParser } from 'fast-xml-parser'
const parser = new XMLParser();

const log = Logger.default('trichter-scraper/aglink')


interface IFeedEntry {
  title: string
  link: string
  published: string
  updated: string
  id: string
  content: string
  author: any
  summary: string
}


const crawl: ScrapeFunction = async (options?: {}) => {
  try {
    const res = await fetch('https://ag-link.xyz/feed.xml')
    const body = await res.text()
    const events: ScrapedEvent[] = []

    const data = parser.parse(body)
    for (const entry of data.feed.entry as IFeedEntry[]) {
      if (entry.id.indexOf('/event/') !== 0) continue

      const description = html2markdown(entry.content)

      // is date in the past?
      if (moment(entry.published).isBefore(moment())) continue

      const r = description.match(/findet am .*? um (?<hour>[0-9]{2}):(?<minutes>[0-9]{2}) Uhr (in|im) (?<location>.*?) statt/)

      if (!r) {
        log.warn(`could not get time and location of ${entry.id}`)
        continue
      }

      const event: ScrapedEvent = {
        id: entry.id,
        title: entry.title,
        start: entry.published.replace(/T\d{2}:\d{2}/, `T${r.groups.hour}:${r.groups.minutes}`),
        end: null,
        address: r.groups.location,
        link: `https://ag-link.xyz${entry.id}.html`,
        teaser: entry.summary.replace(/\n/mg, ''),
        description: description
      }
      events.push(event)
    }
    return events
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
