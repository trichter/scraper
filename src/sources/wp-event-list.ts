import { ScrapeFunction, ScrapedEvent } from './common.js'
import moment from 'moment'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'
import showdown from 'showdown'
import * as jsdom from 'jsdom'
const log = Logger.default('trichter-scraper/wp-event-list')

const converter = new showdown.Converter({
  encodeEmails: false
})
const dom = new jsdom.JSDOM()


const crawl: ScrapeFunction = async (options: {url: string}) => {
  if(!options.url) {
      log.error(new Error('scraper needs an URL'))
      return []
  }
  try {
    const res = await fetch(options.url)
    const $ = cheerio.load(await res.text())
    let out = []
    $('li.event').each((i,el) => {
        const title = $('.event-title', el).text()
        const link = $('.event-title a', el).attr('href')
        const day = $('.event-day', el).text()
        const month = $('.event-month', el).text()
        const year = $('.event-year', el).text()
        const time = $('.event-time', el).text()
        const location = $('.event-location', el).text()
        const content = $('.event-content', el).html()

        const start = moment(`${day}. ${month} ${year}, ${time.replace('Uhr', '').trim()}`, 'DD. MMM YYYY, HH').toISOString()
        const description = converter.makeMarkdown(content, dom.window.document).replace(/<br>/g, '')

        const event: ScrapedEvent = {
            id: link.split('=')[1],
            title: title,
            start: start,
            end: null,
            address: location,
            link: link,
            description: description
        }
        out.push(event)
    })

    console.log(out)

    return out
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
