import { ScrapeFunction, ScrapedEvent } from './common.js'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'
import showdown from 'showdown'
import * as jsdom from 'jsdom'
const log = Logger.default('trichter-scraper/wp-event-organiser')

const converter = new showdown.Converter({
  encodeEmails: false
})
const dom = new jsdom.JSDOM()


const crawl: ScrapeFunction = async (options: {website: string}) => {
  if(!options.website) {
      log.error(new Error('scraper needs a website url'))
      return []
  }
  try {
    const res = await fetch(options.website+'/wp-admin/admin-ajax.php?action=eventorganiser-fullcal&start=2020-09-28')
    const data = await res.json()
    const cache = {}
    let out = []
    for(let e of data as any[]) {
        if(!cache[e.url]) {
            const res2 = await fetch(e.url)
            const body = await res2.text()
            const $ = cheerio.load(body)
            const eventJson = JSON.parse(body.match(/<script type="application\/ld\+json">([\s\S]+?)<\/script>/m)[1].trim())
            const image = eventJson.image
            $('.eventorganiser-event-meta').remove()
            const description = $('.entry-content').html().replace(/<!--.*?-->/mg, '').replace(/(<div.*?>|<\/div>)/g, '').trim()
            cache[e.url] = {
                locationName: eventJson.location.name,
                description: converter.makeMarkdown(description, dom.window.document).replace(/<br>/g, '')
            }

            if(eventJson.location.address) {
                cache[e.url].address = eventJson.location.address.streetAddress || ''
                if(cache[e.url].address && eventJson.location.address.addressLocality) {
                    cache[e.url].address += ', '
                }
                cache[e.url].address += eventJson.location.address.addressLocality || ''
            }
            if(image) {
                let i = image.split('/')
                try {
                    const imageRes = await fetch(image)
                    const contentType = imageRes.headers.get('content-type')
                    if (contentType && contentType.match(/^image\/(jpeg|png)$/)) {
                      const image = await imageRes.buffer()
                      cache[e.url].imageData = image.toString('base64')
                      cache[e.url].image = i[i.length-1]
                    }
                } catch(err) {
                    log.error(err)
                }
            }
        }
        const details = cache[e.url]

        const slug = e.url.match(/\/event\/(.*?)\//)[1]
        let event: ScrapedEvent = {
            id: slug+'_'+e.start.slice(0,10),
            title: e.title,
            start: e.start,
            end: e.end,
            locationName: details.locationName,
            address: details.address,
            link: e.url,
            description: details.description,
            image:details.image,
            imageData: details.imageData
        }
        out.push(event)
    }

    return out
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
