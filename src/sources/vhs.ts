import { ScrapeFunction, ScrapedEvent } from './common.js'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import { XMLParser } from 'fast-xml-parser'
const parser = new XMLParser();

import { URLSearchParams } from 'url'

const log = Logger.default('trichter-scraper/vhs')

const crawl: ScrapeFunction = async (options?: {}) => {
    // TODO
    const params = new URLSearchParams({
        'position': '',
        'umkreis': '50',
        'text': '',
        'text_filter': '',
        'kategorien': '1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.09,1.10,1.11',
        'kategorien_filter': '',
        'veranstalter_filter': '14001',
        'von_datum': '2023-03-01',
        'bis_datum': '2023-12-31',
        'tageszeiten': '',
        'wochentage': '',
        'kursart': 'vor_ort',
        'count': '50',
        'startIndex': '1',
        'sortierung': 'entfernung'
    })
    const res = await fetch('https://api.volkshochschule.de/2/veranstaltungen/suche?'+params.toString())
    const xml = parser.parse(await res.text())
    const events: ScrapedEvent[] = []
    for(let e of xml.response.items.veranstaltung) {
        if(e.preis?.betrag) continue 
        console.log(e)
    }
    // console.log(xml.response)
    return events
}
export default crawl
