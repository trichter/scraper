import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'
import { ScrapeFunction, ScrapedEvent, fetchImage, html2markdown } from './common.js'
import { parse, formatISO } from 'date-fns'

const log = Logger.default('trichter-scraper/ostpassagetheater')

const crawl: ScrapeFunction = async (options?: {}) => {
    const res = await fetch('https://ost-passage-theater.de/')
    const html = await res.text()
    const events: ScrapedEvent[] = []
    const matches = html.matchAll(/^\s*<a href="(https:\/\/ost-passage-theater.de\/veranstaltungen\/.*?)"/mg)

    const links: string[] = []
    for(let m of matches) {
        const link = m[1].split('?')[0]
        if(!links.includes(link)) links.push(link)
    }

    for(let link of links) {
        try {
            const res = await fetch(link)
            const $ = cheerio.load(await res.text())
            const title = $('.event-title').text()
            const dates= $('.event-reservation > .row')
    
            const id = $('a[title="SMS senden"]').attr('href').match(/id=(\d+)/)[1]
            log.debug('scraping '+link)
            for(let el of dates.toArray()) {
                // const resLink = $('.reservation-link > a', el).attr('href')
                const timeStr = $('.reservation-time', el).text()
                const time = parse(timeStr.split(', ')[1].replace(/Uhr/,'').trim(), "dd.MM.yyyy HH:mm", new Date())
                const price = $('.reservation-price', el).text()
                if(price?.includes('€')) continue // skip non-free dates
    
                let description = html2markdown($('.event-description').html().replace(/<hr>/g, ''))
    
                const durationEl = $('.event-duration')
                if(durationEl) {
                    $('b', durationEl).remove()
                    const duration = durationEl.text().trim()
                    if(duration) {
                        description += '\n\n**Dauer:** '+duration
                    }
                }
    
                const event: ScrapedEvent = {
                    id: id+'-'+Math.round(time.getTime()/1000),
                    parentId: id,
                    title: title,
                    link,
                    start: formatISO(time),
                    locationName: 'Ost-Passage Theater',
                    address: 'Konradstr. 27',
                    description: description
                }
    
                const imgSrc = $('.event-image img').attr('data-img-src')
                if(imgSrc) {
                    const i = await fetchImage(imgSrc)
                    event.image = i.image
                    event.imageData = i.imageData
                }
                events.push(event)
            }
        } catch(err) {
            log.error(err)
        }
    }
    return events
}
export default crawl
