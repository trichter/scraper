import showdown from 'showdown'
import * as jsdom from 'jsdom'
import { basename } from 'path'
import fetch from '../fetch.js'
import moment from 'moment'
import ical from 'ical'


export interface ScrapedEvent {
  id: string
  parentId?: string
  title: string
  start: string
  end?: string
  locationName?: string
  address: string
  link?: string
  image?: string
  imageData?: string
  teaser?: string
  description?: string
}
export type ScrapeFunction = (options: any) => Promise<ScrapedEvent[]>

const converter = new showdown.Converter({
    encodeEmails: false
})
const dom = new jsdom.JSDOM()

// https://stackoverflow.com/a/17076120
export function decodeHTMLEntities (text: string): string {
    var entities = [
      ['amp', '&'],
      ['apos', '\''],
      ['#x27', '\''],
      ['#x2F', '/'],
      ['#39', '\''],
      ['#47', '/'],
      ['lt', '<'],
      ['gt', '>'],
      ['nbsp', ' '],
      ['quot', '"']
    ]
  
    for (var i = 0, max = entities.length; i < max; ++i) { text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]) }
  
    return text
  }

// TODO: how to merge decodeCharCodes & decodeHTMLEntities?
function decodeCharCodes(str: string) {
    return str.replace(/&#[0-9]+;/g, (a) => {
        return String.fromCharCode(parseInt(a.slice(2, a.length-1)))
    })
}
  
  
export function html2markdown(str: string) {
    return converter.makeMarkdown(decodeHTMLEntities(str), dom.window.document)
        .replace(/<br>/g, "")
        // .replace(/<\/?[^>]+(>|$)/g, " ") // this is no secure XSS mitigation!
        // .replace(/[ ]{2,}/g, ' ')
        .trim()
}

export async function fetchImage(url: string) {
    const u = new URL(url)
    const imageRes = await fetch(u.href)
    const contentType = imageRes.headers.get('content-type')
    if (contentType && contentType.match(/^image\/(jpeg|png)$/)) {
      const image = await imageRes.buffer()
      return {
          imageData: image.toString('base64'),
          image: decodeURIComponent(basename(u.pathname))
            .replace(/ /g, '_')
      }
    } else {
      console.log('got invalid content type for image', contentType)
    }
}



export async function scrapeIcal (url: string, options: any = {}): Promise<ScrapedEvent[]> {
  const res = await fetch(url, options)
  const data = await res.text()
  const cal = ical.parseICS(data, options)
  const events = Object.keys(cal).map((id) => {
      const c = cal[id]
      if (!c.summary) return null
      const event: ScrapedEvent = {
        id: id.split('@')[0],
        title: decodeHTMLEntities(c.summary),
        start: moment(c.start).toISOString(),
        end: c.end ? moment(c.end).toISOString() : null,
        address: c.location || null,
        link: typeof c.url === 'object' ? c.url.val : c.url,
        description: c.description ? decodeHTMLEntities(decodeCharCodes(c.description)) : null
      }
      return event
  })
  
  return events.filter((e) => {
      if (!e) return false
      if (moment(e.start).isBefore(moment())) return false
      return true
  })
}