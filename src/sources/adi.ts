import moment from 'moment'
import { ScrapeFunction, ScrapedEvent, fetchImage, html2markdown } from './common.js'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'

const log = Logger.default('trichter-scraper/adi')

const crawl: ScrapeFunction = async (options?: {}) => {
    const res = await fetch('https://adi-leipzig.net/')
    const $ = cheerio.load(await res.text())
    const events: ScrapedEvent[] = []

    const cal = $('#aditue > div')


    const now = new Date
    let lastDate = null

    const dates: Array<{time: moment.Moment, title: string, link: string}> = []
    cal.each((i, el) => {
        for(let e of $(el).children().get()) {
            switch(e.attribs.class) {
                case 'adi_date':
                    const dateStr = $('span', e).first().text()
                    const d = dateStr.split('.')
                    let year = now.getUTCFullYear()
                    if(now.getMonth() > 10 && d[1][0] == '0') year++
                    lastDate = [year, d[1], d[0]].join('-')
                    break
                case 'adi_time':
                    const timeStr = $('span', e).first().text()
                    const title = $('span', e).last().text()
                    const link = $('a', e).attr('href')
                    
                    const time = moment(lastDate+" "+timeStr)
                    dates.push({time, title, link})
                    break
            }
        }
    })

    for(let d of dates) {
        try {
            const res = await fetch(d.link)
            const $ = cheerio.load(await res.text())
            const description = html2markdown($('[data-testid="event-permalink-details"]').html() || $('.entry-content').html())
            const imageUrl = $('.post-thumbnail > img').attr('src')
    
            const event:  ScrapedEvent = {
                id: res.url.split('/')[3]+'-'+d.time.toISOString().slice(0,10),
                title: d.title,
                start: d.time.toISOString(),
                address: "Georg-Schwarz-Straße 19",
                link: res.url,
                description: description
            }
            if(imageUrl) {
                // image?: string
                // imageData?: string
                const image = await fetchImage(imageUrl)
                if(image) {
                    event.image = image.image
                    event.imageData = image.imageData
                }
            }
    
            events.push(event)
        } catch(err) {
            log.error(err)
        }
    }

    return events
}
export default crawl
