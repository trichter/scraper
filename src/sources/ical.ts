import { ScrapeFunction, scrapeIcal } from './common.js'
import Logger from '@trichter/logger'
const log = Logger.default('trichter-scraper/ical')
import * as https from 'https'

// TODO: use image attachments

const crawl: ScrapeFunction = async (options: {url: string, httpsRejectUnauthorized?: boolean}) => {
  if (!options || !options.url) throw new Error('option \'url\' must be provided')
  try {
    const reqOptions: any  = {}
    if(options.httpsRejectUnauthorized === false) {
      const httpsAgent = new https.Agent({
        rejectUnauthorized: false,
      });
      reqOptions.agent = httpsAgent
    }
    log.debug(`load ${options.url}`)
    return scrapeIcal(options.url, reqOptions)
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
