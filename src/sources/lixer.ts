import moment from 'moment'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import { ScrapeFunction, ScrapedEvent, fetchImage } from './common.js'

const log = Logger.default('trichter-scraper/lixer')

interface LixerTime {
    year: number
    month: number
    day: number
    time: string
}
interface LixerEvent {
    starts_at: LixerTime,
    finishes_at: string
    img: string
    title: string
    text: string
}

function lixerTimeToMoment(t: LixerTime): moment.Moment {
    return moment({
        year: t.year,
        month: t.month,
        day: t.day,
        hours: parseInt(t.time.split(':')[0]),
        minutes: parseInt(t.time.split(':')[1])
    })
}

const crawl: ScrapeFunction = async (options?: {}) => {
    const events: ScrapedEvent[] = []
    
    const thisMonth = new Date()
    const nextMonth = new Date()
    nextMonth.setMonth(nextMonth.getMonth()+1)

    const months = [
        `${thisMonth.getFullYear()}-${(thisMonth.getMonth()+1)}`,
        `${nextMonth.getFullYear()}-${(nextMonth.getMonth()+1)}`
    ]
    for(let month of months) {
        const res = await fetch(`https://zschocher.com/get/agenda/${month}`)
        const data = await res.json() as LixerEvent[]
        
        for(let e of data) {
            try {
                let id = e.starts_at.year + '-' + e.starts_at.month + '-' + e.starts_at.day;
                
                let i
                if(e.img) {
                    try {
                        i = await fetchImage('https://zschocher.com'+e.img.replace('public/', ''))
                        id += '-' + i.image.split('.')[0]        
                    } catch(err) {
                        log.error(err)
                    }
                } else {
                    // append title
                    id += '-'+e.title.replace(/ /g, '').toLowerCase();
                }
                const event: ScrapedEvent = {
                    id: id,
                    title: e.title,
                    start: lixerTimeToMoment(e.starts_at).toISOString(),
                    end: e.finishes_at ? lixerTimeToMoment({
                        ...e.starts_at,
                        time: e.finishes_at
                    }).toISOString() : null,
                    address: 'Pörstenerstr. 9',
                    description: e.text
                }
                events.push(event)
            } catch(err) {
                log.error(err)
            }
        }
    }
    
    return events
}
export default crawl
 