import { ScrapeFunction, ScrapedEvent, scrapeIcal, html2markdown } from './common.js'
import Logger from '@trichter/logger'
import fetch from '../fetch.js'
import cheerio from 'cheerio'

const log = Logger.default('trichter-scraper/boell')

const crawl: ScrapeFunction = async (options?: { filter: string[] }) => {

    const querystring = options.filter.map((v,index) => `f[${index}]=${encodeURIComponent(v)}`).join('&')

    const res = await fetch('https://calendar.boell.de/de/calendar/advancedsearch?'+querystring)
    const html = await res.text()
    const matches = html.match(/data-history-node-id="\d+"/g)
    if(!matches) return []
    const ids = matches.map(s => s.slice(22, -1))
    
    const events: ScrapedEvent[] = []
    for(let id of ids) {
        
        const event = (await scrapeIcal(`https://calendar.boell.de/de/node/${id}/savetoCalendar`))[0]
        if(!event) continue
        event.id = id
        event.link = `https://calendar.boell.de/de/node/${id}/`
        event.title = event.title.replace(/ \| $/, '') // there is sometimes a " | " at the end
        const res = await fetch(`https://calendar.boell.de/de/node/${id}/`)
        const $ = cheerio.load(await res.text())
        const descEl = $('.event--content > .column > div')
        const description = descEl.html()
        const locationStr = descEl.text().match(/Ort:(.*)/i)?.[1]
            .trim()
            .replace(/\s*\/\/\s*/g, ', ')
        if(locationStr) {
            const l = locationStr.split(', ')
            if(l.length > 1) {
                event.locationName = l[0]
                event.address = l.slice(1).join(', ')
            } else {
                event.address = locationStr
            }
        }
        event.description = html2markdown(description)
        events.push(event)
    }
    return events
}
export default crawl
 